const loggedInUserToken = async (app) => {
    const response = await app
        .post('/login')
        .send({
            username: 'sytec',
            password: 'test12345',
        })
        .expect(200);

    return response.body.token;
};

module.exports = {
    loggedInUserToken,
};
