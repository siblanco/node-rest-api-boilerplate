const supertest = require('supertest');
const app = require('../app');

const api = supertest(app);

describe('GET /users', () => {
    test('displays a total of 2 users', async () => {
        const users = await api.get('/users');
        expect(users.body).toHaveLength(2);
    });

    test('displays specific user with posts', async () => {
        const user = await api.get('/users/1');
        expect(user.body.id).toBe(1);
        expect(user.body.posts).toHaveLength(2);
    });
});

describe('POST /users', () => {
    let user = {
        name: 'Chuck norris',
        password: 'yupupy',
        email: 'chuck@gmail.com',
        username: 'destroyer',
    };

    test('creates a new user with valid data', async () => {
        await api
            .post('/users')
            .send(user)
            .expect(201)
            .expect('Content-Type', /application\/json/);

        const users = await api.get('/users');
        expect(users.body).toHaveLength(3);

        expect(users.body[2].username).toBe('destroyer');
        expect(users.body[2].name).toBe('Chuck norris');
    });

    test('fails with status 400 if invalid data', async () => {
        delete user.email;
        await api.post('/users').send(user).expect(400);

        const users = await api.get('/users');
        expect(users.body).toHaveLength(2);
    });

    test('fails with status 400 if email already exists', async () => {
        user.email = 'hallo@siblanco.dev';
        await api.post('/users').send(user).expect(400);

        const users = await api.get('/users');
        expect(users.body).toHaveLength(2);
    });
});
