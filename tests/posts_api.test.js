const supertest = require('supertest');
const app = require('../app');
const { loggedInUserToken } = require('./helpers');

const api = supertest(app);

describe('GET /posts', () => {
    test('displays a total of 3 posts', async () => {
        const posts = await api.get('/posts');
        expect(posts.body).toHaveLength(3);
    });

    test('returns a specific post', async () => {
        const post = await api.get('/posts/1');
        expect(post.body.id).toBe(1);
    });

    test('fails with status 404 if post does not exist', async () => {
        await api.get('/posts/5').expect(404);
    });

    test('fails with 400 if id is not valid', async () => {
        await api.get('/posts/aisja').expect(400);
    });
});

describe('POST /posts', () => {
    let post = {
        title: 'I am new!',
        slug: 'i-am-new',
    };

    test('adds a new post with 0 likes if data is valid', async () => {
        const token = await loggedInUserToken(api);
        await api
            .post('/posts')
            .set('Authorization', `bearer ${token}`)
            .send(post)
            .expect(201)
            .expect('Content-Type', /application\/json/);

        const posts = await api.get('/posts');

        expect(posts.body).toHaveLength(4);
        expect(posts.body[3].title).toBe('I am new!');
        expect(posts.body[3].likes).toBe(0);
    });

    test('fails with status 400 if invalid data', async () => {
        const token = await loggedInUserToken(api);
        delete post.title;
        await api
            .post('/posts')
            .set('Authorization', `bearer ${token}`)
            .send(post)
            .expect(400);
    });
});

describe('DELETE /posts', () => {
    test('succeeds with status 204 if id is valid', async () => {
        const token = await loggedInUserToken(api);
        await api
            .delete('/posts/1')
            .set('Authorization', `bearer ${token}`)
            .expect(204);

        const posts = await api.get('/posts');
        expect(posts.body).toHaveLength(2);
    });

    test('fails with staus 400 if id is not valid', async () => {
        const token = await loggedInUserToken(api);
        await api
            .delete('/posts/xoxo')
            .set('Authorization', `bearer ${token}`)
            .expect(400);

        const posts = await api.get('/posts');
        expect(posts.body).toHaveLength(3);
    });
});

describe('PUT /posts', () => {
    const updatedPost = {
        title: 'I am a new title',
        slug: 'i-am-a-new-title',
    };

    test('updates post with status 201 and returns updated post', async () => {
        const token = await loggedInUserToken(api);
        const posts = await api.get('/posts');
        const oldPostId = posts.body[0].id;

        const updatePost = await api
            .put(`/posts/${oldPostId}`)
            .set('Authorization', `bearer ${token}`)
            .send(updatedPost);

        expect(updatePost.body.title).toBe('I am a new title');
        expect(posts.body[0].title).not.toBe('I am a new title');

        const newPostList = await api.get('/posts');
        expect(newPostList.body[0].title).toBe('I am a new title');
    });

    test('updates only included parameters', async () => {
        const token = await loggedInUserToken(api);
        const unModifiedPost = await api.get('/posts');
        const unModifiedSlug = unModifiedPost.body[0].slug;

        await api
            .put('/posts/1')
            .set('Authorization', `bearer ${token}`)
            .send({ title: 'I am a new titleeee' });

        const posts = await api.get('/posts');
        expect(posts.body[0].title).toBe('I am a new titleeee');
        expect(posts.body[0].slug).toBe(unModifiedSlug);
    });
});

describe('MIDDLEWARE /posts', () => {
    const post = {
        title: 'New post',
        slug: 'new-post'
    };

    test('fails with 401 when not logged in', async () => {
        await api.put('/posts/1').send(post).expect(401);
        await api.post('/posts').send(post).expect(401);
        await api.delete('/posts/1').expect(401);
    });
    
    test('fails with status 400 if id is not valid', async () => {
        await api.get('/posts/yaya').expect(400);
        await api.delete('/posts/xoxo').expect(400);
        await api.put('/posts/xoxo').expect(400);

        // verify database still contains 3 posts
        const posts = await api.get('/posts');
        expect(posts.body).toHaveLength(3);
    });
});
