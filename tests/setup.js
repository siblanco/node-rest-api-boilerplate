const knex = require('../db/knex');

// migrate test DB
beforeAll(() => knex.migrate.latest());

// clean up and seed before every test
beforeEach(() => knex.seed.run());

// close db connection
afterAll(() => knex.destroy());
