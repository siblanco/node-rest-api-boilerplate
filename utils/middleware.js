const logger = require('./logger');
const { customError } = require('./helper');

const requestLogger = (req, res, next) => {
    logger.info('Method:', req.method);
    logger.info('Path:  ', req.path);
    logger.info('Body:  ', req.body);
    logger.info('---');
    next();
};

const unknownEndpoint = (req, res) => {
    res.status(404).send({ error: 'unknown endpoint' });
};

const errorHandler = (err, req, res, next) => {
    logger.error('Error:', err.name, err.message);

    if (err.name === 'NotNullViolationError') {
        return res.status(400).send({ error: 'missing data' });
    } else if (err.name === 'ValidationError') {
        return res.status(400).json({ error: err.message });
    } else if (err.name === 'ForeignKeyViolationError') {
        return res.status(409).json({ error: err.name });
    } else if (err.name === 'JsonWebTokenError') {
        return res.status(401).json( {error: 'invalid token'} )
    }

    next(err);
};

const validateId = (req, res, next) => {
    const { id } = req.params;

    // validate its a number
    if (id % 1 !== 0) {
        customError('ValidationError', 'ID is not a number');
    }

    next();
};

const tokenExtractor = (req, res, next) => {
    const authorization = req.get('authorization');

    if (authorization && authorization.toLowerCase().startsWith('bearer ')) {
        req.token = authorization.substring(7);
    }
    next();
};

module.exports = {
    requestLogger,
    unknownEndpoint,
    errorHandler,
    validateId,
    tokenExtractor,
};
