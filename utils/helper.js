const jwt = require('jsonwebtoken');

const customError = (name, msg) => {
    const error = new Error(msg);
    error.name = name;
    throw error;
};

const isValidToken = (token) => {
    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!token || !decodedToken.data.id) {
        return false;
    }

    return decodedToken;
};

module.exports = {
    customError,
    isValidToken
};
