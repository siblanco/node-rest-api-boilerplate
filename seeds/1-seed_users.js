const bcrypt = require('bcrypt');

exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('users')
        .del()
        .then(async function () {
            // Inserts seed entries
            return knex('users').insert([
                { id: 1, username: 'sytec', name: 'Hassan', email: 'hallo@siblanco.dev', password: await bcrypt.hash('test12345', 10) },
                { id: 2, username: 'lisa', name: 'Lisa', email: 'hi@siblanco.dev', password: await bcrypt.hash('test123', 10) },
            ]);
        });
};
