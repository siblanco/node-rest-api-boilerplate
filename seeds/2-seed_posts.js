exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('posts')
        .del()
        .then(function () {
            // Inserts seed entries
            return knex('posts').insert([
                { id: 1, title: 'Hello World', slug: 'hello-world', likes: 5, created_at: knex.fn.now(), userId: 1 },
                { id: 2, title: 'Supafly', slug: 'supafly', likes: 7, created_at: knex.fn.now(), userId: 1 },
                { id: 3, title: 'Tonight', slug: 'tonight', likes: 1, created_at: knex.fn.now(), userId: 2 },
            ]);
        });
};
