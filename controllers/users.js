const bcrypt = require('bcrypt');

const usersRouter = require('express').Router();
const User = require('../models/User');
const { validateId } = require('../utils/middleware');

usersRouter.all('/:id', validateId);

usersRouter.get('/', async (req, res) => {
    const users = await User.query();
    res.json(users);
});

usersRouter.get('/:id', async (req, res) => {
    const { id } = req.params;
    await User.query()
        .findById(id)
        .withGraphJoined('posts')
        .then((user) => res.json(user));
});

usersRouter.post('/', async(req, res) => {
    const {username, name, email, password} = req.body;
    const hashedPW = await bcrypt.hash(password, 10);

    const user = await User.query().insert({
        username,
        name,
        email,
        password: hashedPW
    });
    
    res.status(201).json(user);
});

module.exports = usersRouter;
