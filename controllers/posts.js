const postsRouter = require('express').Router();

const Post = require('../models/Post');
const User = require('../models/User');
const middleware = require('../utils/middleware');
const { isValidToken } = require('../utils/helper');

postsRouter.all('/:id', middleware.validateId);

postsRouter.get('/', async (req, res) => {
    const posts = await Post.query()
        .withGraphJoined('author')
        .orderBy('posts.id');
    res.json(posts);
});

postsRouter.get('/:id', async (req, res) => {
    const { id } = req.params;

    const post = await Post.query().findById(id);

    if (post) {
        return res.json(post);
    } else {
        return res.status(404).end();
    }
});

postsRouter.delete('/:id', middleware.tokenExtractor, async (req, res) => {
    const { token } = req;
    const { id } = req.params;

    const decodedToken = isValidToken(token);
    if (!decodedToken) {
        return res.status(401).json({ error: 'token missing or not valid' });
    }

    const post = await Post.query().findById(id);
    if (post && post.userId !== decodedToken.data.id) {
        return res
            .status(403)
            .json({ error: 'you are not authorized to do this' });
    }

    await Post.query().deleteById(id);
    res.status(204).end();
});

postsRouter.post('/', middleware.tokenExtractor, async (req, res) => {
    const { token } = req;

    const decodedToken = isValidToken(token);
    if (!decodedToken) {
        return res.status(401).json({ error: 'token missing or not valid' });
    }

    const user = await User.query().findById(decodedToken.data.id);
    const post = await Post.query().insert({
        title: req.body.title,
        slug: req.body.slug,
        likes: 0,
        userId: user.id,
    });

    res.status(201).json(post);
});

postsRouter.put('/:id', middleware.tokenExtractor, async (req, res) => {
    const { token } = req;
    const { id } = req.params;

    const decodedToken = isValidToken(token);
    if (!decodedToken) {
        return res.status(401).json({ error: 'token missing or not valid' });
    }

    const post = await Post.query().findById(id);
    if (post && post.userId !== decodedToken.data.id) {
        return res
            .status(403)
            .json({ error: 'you are not authorized to do this' });
    }

    const { title, slug } = req.body;
    const updatedPost = await Post.query().patchAndFetchById(id, {
        title,
        slug,
    });

    if (updatedPost) {
        res.status(201).json(updatedPost);
    } else {
        res.status(404).end();
    }
});

module.exports = postsRouter;
