const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const loginRouter = require('express').Router();
const User = require('../models/User');

loginRouter.post('/', async (req, res) => {
    const { username, password } = req.body;

    const [user] = await User.query().where('username', username);
    const isValid = await bcrypt.compare(password, user.password);

    if (!(isValid && user)) {
        return res.status(401).json({
            error: 'invalid username or password',
        });
    }

    const userForToken = {
        id: user.id,
        username: user.username,
        email: user.email,
    };

    const token = jwt.sign(
        {
            data: userForToken,
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
        },
        process.env.SECRET
    );

    res.json({ token, user });
});

module.exports = loginRouter;
