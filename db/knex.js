require('dotenv').config();
const dbconfig = require('../knexfile');

let knex;

if(process.env.NODE_ENV === 'test') {
    knex = require('knex')(dbconfig['test']);
} else if(process.env.NODE_ENV === 'production') {
    knex = require('knex')(dbconfig['production']);
} else {
    knex = require('knex')(dbconfig['development']);
}

module.exports = knex;
