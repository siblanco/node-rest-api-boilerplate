exports.up = function (knex) {
    return knex.schema.table('posts', (t) => {
        t.integer('userId')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('users')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
    });
};

exports.down = function (knex) {
    return knex.schema.table('posts', (t) => {
        t.dropColumn('userId');
    });
};
