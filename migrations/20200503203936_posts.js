exports.up = function (knex) {
    return knex.schema.createTable('posts', (t) => {
        t.increments('id').primary();
        t.string('title').notNullable();
        t.string('author').notNullable();
        t.string('slug').notNullable();
        t.integer('likes');
        t.timestamp('created_at').defaultTo(knex.fn.now());
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('posts');
};
