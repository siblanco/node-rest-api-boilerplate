exports.up = function (knex) {
    return knex.schema.table('posts', (t) => {
        t.dropColumn('author');
    });
};

exports.down = function (knex) {
    return knex.schema.table('posts', (t) => {
        t.string('author').notNullable();
    });
};
