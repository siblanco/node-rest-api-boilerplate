const express = require('express');
const app = express();

const User = require('./models/User');

// lets us drop try / catch blocks and automatically calls next(err) if there is an error
// looks awesome to me
require('express-async-errors');

const cors = require('cors');
const postsRouter = require('./controllers/posts');
const usersRouter = require('./controllers/users');
const loginRouter = require('./controllers/login');
const middleware = require('./utils/middleware');

app.use(cors());
app.use(express.json());
app.use(middleware.requestLogger);

app.get('/', (req, res) =>
    res.send(
        '<h1>node-rest-api-boilerplate with knex, objection and express</h1>'
    )
);

app.use('/login', loginRouter);
app.use('/posts', postsRouter);
app.use('/users', usersRouter);

app.use(middleware.unknownEndpoint);
app.use(middleware.errorHandler);

module.exports = app;
