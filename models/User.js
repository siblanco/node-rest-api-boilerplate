const { Model } = require('objection');
const knex = require('../db/knex');

const { customError } = require('../utils/helper');

Model.knex(knex);

class User extends Model {
    static get tableName() {
        return 'users';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['username', 'name', 'password', 'email'],
            properties: {
                id: { type: 'integer' },
                username: { type: 'string', maxLength: 255 },
                name: { type: 'string', maxLength: 255 },
                email: {
                    type: 'string',
                    pattern:
                        '[a-z0-9._%+!$&*=^|~#%{}/-]+@([a-z0-9-]+.){1,}([a-z]{2,22})',
                },
                password: { type: 'string', maxLength: 255 },
            },
        };
    }

    $formatJson(json) {
        json = super.$formatJson(json);
        delete json.password;
        return json;
    }

    $beforeInsert() {
        return this.$beforeSave(true);
    }

    $beforeUpdate() {
        return this.$beforeSave(false);
    }

    $beforeSave(inserting) {
        return this.constructor
            .query()
            .select('id')
            .where('email', this.email)
            .first()
            .then((row) => {
                if (typeof row === 'object' && row.id) {
                    if (
                        inserting ||
                        Number.parseInt(row.id) !== Number.parseInt(this.id)
                    ) {
                        customError(
                            'ValidationError',
                            `Email is already in use`
                        );
                    }
                }
            });
    }

    static get relationMappings() {
        const Post = require('./Post');
        return {
            posts: {
                relation: Model.HasManyRelation,
                modelClass: Post,
                join: {
                    from: 'users.id',
                    to: 'posts.userId',
                },
            },
        };
    }
}

module.exports = User;
