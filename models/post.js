const { Model } = require('objection');
const knex = require('../db/knex');

Model.knex(knex);

class Post extends Model {
    static get tableName() {
        return 'posts';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['title', 'slug', 'userId'],
            properties: {
                id: { type: 'integer' },
                title: { type: 'string', maxLength: 255 },
                slug: { type: 'string', maxLength: 255 },
                userId: { type: 'integer' },
            },
        };
    }

    $formatJson(json) {
        json = super.$formatJson(json);
        delete json.userId;
        return json;
    }

    static get relationMappings() {
        const User = require('./User');

        return {
            author: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'posts.userId',
                    to: 'users.id',
                },
            },
        };
    }
}

module.exports = Post;
